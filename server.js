const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const userRouter = require("./app/request/userRouter");
const adminRouter = require("./app/request/adminRouter");
const uploadRouter = require("./app/request/uploadRouter");
const paymentRouter = require("./app/request/paymentRouter");

const app = express();

// mongoose.connect("mongodb://localhost:27017/machinery")
mongoose.connect(
  "mongodb://Balocodes:balocodes@ds159489.mlab.com:59489/machinery-ng"
);

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/user", userRouter);
app.use("/admin", adminRouter);
app.use("/upload", uploadRouter);
app.use("/payment", paymentRouter);

app.get("/test", (req, res) => {
  res.send("Test route works!");
});

app.listen(
  process.env.PORT || 3333,
  console.log("My app is running on port 3333")
);
