const mongoose = require("mongoose");
let current_time = new Date().toISOString();

let new_schema = new mongoose.Schema({
  advert_name: { type: String, required: true, unique: true},
  advert_image: { type: String },
  advert_area: { type: String },
  sponsor: {type: String },
  status: { type: Boolean, default: false },
  created_at: { type: Date, default: current_time },
  updated_at: { type: Date, default: Date.now }
});

// new_schema.index({
// });

const model = mongoose.model("Advert", new_schema)

module.exports = model