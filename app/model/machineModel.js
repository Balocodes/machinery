const mongoose = require("mongoose");
let current_time = new Date().toISOString();

let new_schema = new mongoose.Schema({
  name: { type: String, required: true },
  owner_id: { type: String },
  description: { type: String },
  category: { type: String },
  features: { type: Object },
  image: { type: String },
  location: { type: String },
  address: { type: String },
  price: { type: Number },
  contact_phone: { type: String },
  contact_email: { type: String },
  classified: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
  created_at: { type: Date, default: current_time, once: true },
  updated_at: { type: Date, default: Date.now }
});

new_schema.index({
    name: 'text',
    category: 'text',
    location: 'text'
});

const model = mongoose.model("Machine", new_schema)

module.exports = model