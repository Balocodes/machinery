const mongoose = require("mongoose");
let current_time = new Date().toISOString();

let new_schema = new mongoose.Schema({
    user_id: { type: mongoose.Schema.Types.ObjectId, ref: "Owner" },
    transaction_ref: { type: String },
    voucher_id: { type: mongoose.Schema.Types.ObjectId, ref: "VoucherModel"}, //if a voucher was used in the transaction
    subscription_id: { type: mongoose.Schema.Types.ObjectId, ref: "SubscriptionPackage"},
    created_at: { type: Date, default: current_time, once: true },
    updated_at: { type: Date, default: Date.now }
  },
  { strict: false }
);


const model = mongoose.model("TransactionModel", new_schema)


module.exports = model;
