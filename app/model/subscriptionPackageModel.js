/**
 * Don't forget to change the model name.
 */

const mongoose = require("mongoose");
let current_time = new Date().toISOString();

let new_schema = new mongoose.Schema({
  subscription_package: { type: String, required: true, unique: true },
  duration: { type: String, required: true },
  price: { type: String },
  features: { type: Object },
  level: { type: Number },
  status: { type: Boolean, default: false },
  created_at: { type: Date, default: current_time, once: true },
  updated_at: { type: Date, default: Date.now }
});

// new_schem.inadex({
// });

const model = mongoose.model("SubscriptionPackage", new_schema)

module.exports = model