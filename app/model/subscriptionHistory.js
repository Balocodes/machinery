const mongoose = require("mongoose");
let current_time = new Date().toISOString();

let new_schema = new mongoose.Schema({
    subscription_id: { type: Boolean },
    subscriber: { type: mongoose.Schema.Types.ObjectId, ref: "Owner" },
    created_at: { type: Date, default: current_time, once: true },
    updated_at: { type: Date, default: Date.now }
  },
  { strict: false }
);

const model = mongoose.model("SubscriptionHistoryModel", new_schema);

module.exports = model;
