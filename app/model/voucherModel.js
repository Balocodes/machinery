const mongoose = require("mongoose");
let current_time = new Date().toISOString();

let new_schema = new mongoose.Schema(
  {
    voucher_name: { type: String, required: true, unique: true },
    voucher_code: { type: String, required: true, unique: true },
    voucher_status: { type: Boolean, default: false },
    voucher_discount: { type: Number, default: 0 }, //percentage to be removed
    voucher_fixed: { type: Number, default: null }, // default amount to remove if not using percentage.
    voucher_details: { type: String },
    created_at: { type: Date, default: current_time, once: true },
    updated_at: { type: Date, default: Date.now }
  },
  { strict: false }
);

const model = mongoose.model("VoucherModel", new_schema)


module.exports = model;
