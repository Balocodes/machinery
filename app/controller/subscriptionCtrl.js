const userModel = require("../model/ownerModel");
const subModel = require("../model/subscriptionPackageModel");
const addSubtractDate = require("add-subtract-date");
const paystack = require("paystack")(
  "sk_test_6ad3199869b40e00987022b9dd3c7be0709367be"
);
const mailCtrl = require("./mailgunCtrl");
const transactionModel = require("../model/transactions");

class SubscriptionCtrl {
  constructor(res) {
    this.res = res;
  }

  checkSubscription(obj) {
    userModel.findById(obj.data._id, (err, data) => {
      if (err) {
        this.res.send({
          message: "An error occured while finding the user",
          code: 500,
          error: true
        });
        return false;
      } else {
        if (data) {
          console.log(data.subscription_end_date);
          if (
            data.subscription_end_date < new Date().toISOString() ||
            data.subscription_end_date == null
          ) {
            this.res.send({
              message: "Sub Expired!",
              code: 401
            });
            return true;
          } else {
            this.res.send({
              result: { "end_data": data.subscription_end_date,
              "start_data": data.subscription_start_date
              },
              message: "Sub Active!",
              code: 200
            });
            return false;
          }
        } else {
          this.res.send({
            message: "Data not found!",
            code: 404,
            error: true
          });
        }
      }
    });
  }

  subscribe(obj) {
    // see if the subscription actually exists.
    subModel.findById(obj.data.sub_id, (sub_err, sub_data) => {
      if (sub_err) {
        this.res.send({
          message: "Error finding subscription",
          code: 500,
          error: true
        });
        return false;
      } else if (sub_data == null) {
        this.res.send({
          message: "Subscription is not available",
          code: 404,
          error: true
        });
        return false;
      } else { // if subscription exists, check if the user exists
        userModel.findById(obj.data.user_id, async (user_err, user_data) => {
          if (user_err) {
            this.res.send({
              message: "Error finding user",
              code: 500,
              error: true
            });
            return false;
          } else if (user_data == null) {
            this.res.send({
              message: "User not found",
              code: 404,
              error: true
            });
            return false;
          } else { // if the user exists, check if the transaction a duplicate
            transactionModel.findOne({transaction_ref: obj.data.transaction_ref}, (trans_err, trans_body) => {
              if(trans_err) {
                this.res.send({message: "Error verifying transaction ref",
                code: 500,
                error: true})
              } else if (trans_body != null ){
                this.res.send({
                  message: "This transaction is not unique.",
                  code: 401,
                  error: true
                })
              } else {
                // Verify the transaction on paystack
                paystack.transaction.verify(obj.data.transaction_ref, async (err, body) => {
                  if(err) {
                    this.res.send({
                      message: "Transaction validation failed",
                      code: 500,
                      error: true
                    })
                  } else if (body.data.status == "success") {
                    // save the transaction
                    let new_transaction = new transactionModel({
                      transaction_ref: obj.data.transaction_ref,
                      user_id: user_data._id,
                      voucher_id: obj.data.voucher_id,
                      subscription_id: sub_data._id
                    })
                    new_transaction.save((new_trans_err, new_trans_data)=> {
                      if(new_trans_err) {
                        this.res.send({
                          message: "Transaction validated but failed to log. Please try again.",
                          code: 500,
                          error: true
                        })
                      }
                    })
                    // update the user's data to reflect the subscription
                    userModel.findByIdAndUpdate(
                      user_data._id,
                      {
                        subscription_package: sub_data.subscription_package,
                        subscription_start_date: new Date(),
                        subscription_end_date: await addSubtractDate.add(
                          new Date(),
                          sub_data.duration,
                          "day"
                        )
                      },
                      (err, data) => {
                        if (err) {
                          console.log(err)
                          this.res.send({
                            message: "An error occured while updating the user",
                            code: 500,
                            error: true
                          });
                        } else {
                          let mail = new mailCtrl(
                            "text",
                            "machinery@machinery.ng",
                            user_data.email,
                            "Subscription Confirmation",
                            `Congratulations. You have successfully subscribed to ${sub_data.subscription_package}`
                          );
                          try {
                            mail.send();
                          } catch(err) {
                            console.log(err)
                            console.log("Failed to send mail")
                          }
                          this.res.send({
                            result: data,
                            message: "Subscription successful",
                            code: 204,
                            error: false
                          });
                        }
                      }
                    );
                  } else {
                    this.res.send({
                      message: "Abandoned transaction",
                      code: 401,
                      error: true
                    })
                  }
                })
              }
            })
          
          }
        });
      }
    });
  }
}

module.exports = SubscriptionCtrl;
