/**
 * This handles stringified data and filters out
 * unneccessary fields
 * @param {Object} obj
 */
function shellMaker(obj) {
    let shell = {
      data: obj.data,
      limit: obj.limit,
      page: obj.page,
      order: obj.order,
      fields: obj.fields,
      find: obj.find,
      search: obj.search
    };
    if (typeof obj.data == "string") {
      shell.data = JSON.parse(obj.data);
    }
    if (typeof obj.find == "string") {
      shell.find = JSON.parse(obj.find);
    }
    if (obj.limit == null) {
      shell.limit = "10";
    }
    if (obj.page == null) {
      shell.page = "0";
    }
    return shell;
  }
  
  /**
   * Checks the mode of the app and decides whether to log data
   * passed into it or not.
   * @param {Object} toLog
   */
  function checkMode(toLog) {
    if (this.mode == "development") {
      console.log(toLog);
    }
    return true;
  }
  
  /**
   * This function takes a string contain properties of an object
   * as well as the object and removes those properties from the 
   * object
   * @param {Object} obj 
   * @param {String} toRemove 
   */
  function filterObject(obj, toRemove) {
    let counter = 0;
    let removeArr = toRemove.split(" ");
    for (x in obj) {
      if(removeArr.includes(x)){
        delete obj[x]
      }
      if(counter >= removeArr.length - 1 ) {
        return obj
      }
    }
    
  }
  
  module.exports = {
    shellMaker: shellMaker,
    checkMode: checkMode,
    objectFilter: filterObject
  };
  