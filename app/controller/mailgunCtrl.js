const Mailgun = require("mailgun-js");
let mailgun = Mailgun({
  apiKey: process.env.MAILGUN_API_KEY,
  domain: process.env.MAILGUN_DOMAIN
});

class MailCtrl {
  /**
   *
   * @param {String} content_type ("text" or "html")
   * @param {String} sender (Email of sender)
   * @param {String} receiver (Email of receiver)
   * @param {String} subject (Subject of email)
   * @param {String} content (The content of the email)
   * @param {Object} response (Express response)
   */
  constructor(
    content_type,
    sender,
    receiver,
    subject,
    content,
    response = null
  ) {
    this.content_type = content_type;
    this.sender = sender;
    this.receiver = receiver;
    this.subject = subject;
    this.content = content;
    this.response = response;
  }

  send() {
    let mailData = {};
    mailData["from"] = this.sender;
    mailData["to"] = this.receiver;
    mailData["subject"] = this.subject;
    mailData[this.content_type] = this.content;

    mailgun.messages().send(mailData, (err, body) => {
      if (err) {
        console.log(err);
        if (this.response) {
          return this.response.send({
            message: "An error occured",
            code: 500,
            error: false
          });
        }
      } else {
        console.log(body);
        if (this.response) {
          this.response.send({
            message: `Email has been sent to ${this.receiver}.`,
            code: 200,
            error: false
          });
        }
      }
    });
  }
}

module.exports = MailCtrl;
