const jwt = require("jsonwebtoken");
const bcryptjs = require("bcryptjs");
const secret = process.env.USER_JWT_SECRET;

class AccessCtrl {
  constructor(model, res = null, mode = "development") {
    this.model = model;
    this.res = res;
    this.mode = mode;
  }

  /**
   * Checks the mode of the app and decides whether to log data
   * passed into it or not.
   * @param {Object} toLog
   */
  checkMode(toLog) {
    if (this.mode == "development") {
      console.log(toLog);
    }
    return true;
  }

  login(obj) {
    this.model.findOne(obj.find, (err, data) => {
      if (err) {
        this.res.send({
          message: "An error occured!",
          code: 500,
          error: true
        });
      } else if(data) {
        try{
          console.log(obj.data.password);
        } catch(err){
          return this.res.send({
            message: "Please make sure data is sent in the correct format!",
            code: 500,
            error: true
          })
        }
        if (bcryptjs.compareSync(obj.data.password, data.password)) {
          let token = jwt.sign(
            {
              user_id: data._id
            },
            secret
          );
          this.res.send({
            result: {
              token: token
            },
            message: "Login Successful",
            code: 200,
            error: false
          });
        } else {
          this.res.send({
            message: "Login failed!",
            code: 401,
            error: true
          });
        }
      } else {
        this.res.send({
          message: "User does not exist!",
          code: 401,
          error: true
        })
      }
    });
  }

  register(obj) {
    obj.data.password = bcryptjs.hashSync(obj.data.password);
    this.model.findOne(obj.find, (err, data) => {
      if (err) {
        this.checkMode(err);
        this.res.send({
          message: "An error occured!",
          code: 500,
          error: true
        });
      } else if (data) {
        this.res.send({
          message: "Data already exists!",
          code: 401,
          error: true
        });
      } else {
        let item = new this.model(obj.data);
        item.save((errr, dataa) => {
          if (errr) {
            this.checkMode(errr);
            this.res.send({
              message: "Failed to add",
              code: 500,
              error: true
            });
          } else {
            this.checkMode(dataa);
            let token = jwt.sign(
              {
                user_id: dataa._id
              },
              secret
            );
            this.res.send({
              result: {
                token: token
              },
              message: "Success",
              code: 200,
              error: false
            });
          }
        });
      }
    });
  }
}

module.exports = AccessCtrl;
