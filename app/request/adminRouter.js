const router = require("express").Router();
const genCtrl = require("../controller/generalCtrl");
const accessCtrl = require("../controller/accessCtrl");
const ownerModel = require("../model/ownerModel");
const adminModel = require("../model/adminModel");
const machineModel = require("../model/machineModel");
const categoryModel = require("../model/categoryModel");
const advertModel = require("../model/advertModel");
const subModel = require("../model/subscriptionPackageModel");
const mode = "development";
const jwt = require("jsonwebtoken");

/**
 * This handles stringified data and filters out 
 * unneccessary fields
 * @param {Object} obj 
 */
function shellMaker(obj){
  let shell = {
    data: obj.data,
    limit: obj.limit,
    page: obj.page,
    order: obj.order,
    fields: obj.fields,
    find: obj.find,
    search: obj.search
  };
  if (typeof obj.data == "string") {
    console.log("converted");
    shell.data = JSON.parse(obj.data);
  }
  if (typeof obj.find == "string") {
    console.log("converted");
    shell.find = JSON.parse(obj.find);
  }
  if (obj.limit == null) {
    shell.limit = "10";
  }
  if (obj.page == null) {
    shell.page = "0";
  }
  return shell;
};

//default route
router.get("/", (req, res) => {
  res.send(true);
});

router.post("/login", async (req, res) => {
  let shell = await shellMaker(req.body);
  new accessCtrl(adminModel, res, mode).login(shell);
})


router.use("*", (req, res, next) => {
  let token = req.body.token || req.query.token;
  try {
    req.decoded = jwt.verify(token, process.env.USER_JWT_SECRET);
    next();
  } catch (error) {
    res.send({
      message: "Verification failed. Please login again.",
      code: 401,
      error: true
    });
  }
});

router.post("/register", async (req, res) => {
  let shell = await shellMaker(req.body);
  new accessCtrl(adminModel, res, mode).register(shell);
})

router.post("/machine", async (req, res) => {
  let shell = await shellMaker(req.body);
  new genCtrl(machineModel, res, mode).addItem(shell);
});

router.get("/machines", async (req, res) => {
  let shell = await shellMaker(req.query);
  new genCtrl(machineModel, res, mode).readItems(shell);
});

router.get("/machine", async (req, res) => {
  let shell = await shellMaker(req.query);
  new genCtrl(machineModel, res, mode).readSingleItem(shell);
});

router.get("/search-machines", async (req, res) => {
  let shell = await shellMaker(req.query)
  new genCtrl(machineModel, res, mode).search(shell)
})

router.put("/machine", async (req, res) => {
  let shell = await shellMaker(req.body);
  new genCtrl(machineModel, res, mode).updateItem(shell);
});

router.delete("/machine", async (req, res) => {
  let shell = await shellMaker(req.body);
  new genCtrl(machineModel, res, mode).deleteItem(shell);
});

router.get("/machine-owners", async (req, res) => {
  let shell = await shellMaker(req.query);
  new genCtrl(ownerModel, res, mode).readItems(shell);
});

router.get("/machine-owner", async(req, res) => {
  let shell = await shellMaker(req.query);
  new genCtrl(ownerModel, res, mode).readSingleItem(shell);
});

router.get("/search-owners", async(req, res) => {
  let shell = await shellMaker(req.query);
  new genCtrl(ownerModel, res, mode).search(shell);
})

router.put("/machine-owner", async (req, res) => {
  let shell = await shellMaker(req.body);
  new genCtrl(ownerModel, res, mode).updateItem(shell);
})

router.delete("/machine-owner", async (req, res) => {
  let shell = await shellMaker(req.body);
  new genCtrl(ownerModel, res, mode).deleteItem(shell);
})

router.post("/classified", async (req, res) => {
  let shell = await shellMaker(req.body);
  new genCtrl(advertModel, res, mode).addItem(shell);
})

router.get("/classifieds", async (req, res) => {
  let shell = await shellMaker(req.query);
  new genCtrl(advertModel, res, mode).readItems(shell);
})

router.get("/classified", async (req, res) => {
  let shell = await shellMaker(req.query);
  console.log(req.query)
  new genCtrl(advertModel, res, mode).readSingleItem(shell);
})

router.put("/classified", async (req, res) => {
  let shell = await shellMaker(req.body);
  new genCtrl(advertModel, res, mode).updateItem(shell);
})

router.delete("/classified", async (req, res) => {
  let shell = await shellMaker(req.body);
  new genCtrl(advertModel, res, mode).deleteItem(shell);
})

router.post("/category", async (req, res) => {
  let shell = await shellMaker(req.body);
  new genCtrl(categoryModel, res, mode).addItem(shell);
})

router.get("/categories", async (req, res) => {
  let shell = await shellMaker(req.query);
  new genCtrl(categoryModel, res, mode).readItems(shell);
})

router.get("/category", async (req, res) => {
  let shell = await shellMaker(req.query);
  new genCtrl(categoryModel, res, mode).readSingleItem(shell);
})

router.put("/category", async (req, res) => {
  let shell = await shellMaker(req.body);
  new genCtrl(categoryModel, res, mode).updateItem(shell);
})

router.delete("/category", async (req, res) => {
  let shell = await shellMaker(req.body);
  new genCtrl(categoryModel, res, mode).deleteItem(shell);
})

router.post("/subscription", async (req, res) => {
  let shell = await shellMaker(req.body);
  new genCtrl(subModel, res, "development").addItem(shell);
})
router.get("/subscriptions", async (req, res) => {
  let shell = await shellMaker(req.query);
  new genCtrl(subModel, res, "development").readItems(shell);
})
router.get("/subscription", async (req, res) => {
  let shell = await shellMaker(req.query);
  new genCtrl(subModel, res, "development").readSingleItem(shell);
})
router.put("/subscription", async (req, res) => {
  let shell = await shellMaker(req.body);
  new genCtrl(subModel, res, "development").updateItem(shell);
})
router.delete("/subscription", async (req, res) => {
  let shell = await shellMaker(req.body);
  new genCtrl(subModel, res, "development").deleteItem(shell);
})

module.exports = router;
