const router = require("express").Router();
const genCtrl = require("../controller/generalCtrl");
const accessCtrl = require("../controller/accessCtrl");
const subCtrl = require("../controller/subscriptionCtrl");
const PaymentCtrl = require("../controller/paymentCtrl");
const ownerModel = require("../model/ownerModel");
const machineModel = require("../model/machineModel");
const categoryModel = require("../model/categoryModel");
const advertModel = require("../model/advertModel");
const subModel = require("../model/subscriptionPackageModel");
const mode = "development";
const jwt = require("jsonwebtoken");

/**
 * This handles stringified data and filters out 
 * unneccessary fields
 * @param {Object} obj 
 */
async function shellMaker(obj) {
  let shell = {
    data: obj.data,
    limit: obj.limit,
    page: obj.page,
    order: obj.order,
    fields: obj.fields,
    find: obj.find,
    search: obj.search,
  };
  if (typeof obj.data == "string") {
    shell.data = await JSON.parse(obj.data);
  }
  if (typeof obj.find == "string") {
    shell.find = await JSON.parse(obj.find);
  }
  if (obj.range) {
    if(typeof obj.range == "string") {
      shell.range = await JSON.parse(obj.range)
    }
    if(shell.data == null) {
      shell.data = {}
    }
    for(let i = 0; i<shell.range.length; i++){
      shell.data[shell.range[i].field] = { $gte: shell.range[i].min, $lte: shell.range[i].max }
    }
  }
  if (obj.limit == null) {
    shell.limit = "10";
  }
  if (obj.page == null) {
    shell.page = "0";
  }
  console.log(shell)
  return shell;
};

//default route
router.get("/", (req, res) => {
  res.send(true);
});

router.post("/register", async (req, res) => {
  let shell = await shellMaker(req.body);
  new accessCtrl(ownerModel, res, mode).register(shell)
})

router.post("/login", async (req, res) => {
  let shell = await shellMaker(req.body);
  shell.find = {
    email: shell.data.email
  }
  new accessCtrl(ownerModel, res, mode).login(shell)
})

router.get("/machine", async (req, res) => {
  let shell = await shellMaker(req.query);
  new genCtrl(machineModel, res, mode).readItems(shell);
});

router.get("/machines", async (req, res) => {
  console.log(req.query)
  let shell = await shellMaker(req.query);
  new genCtrl(machineModel, res, mode).readItems(shell);
});

router.get("/search-machines", async (req, res) => {
  let shell = await shellMaker(req.query)
  new genCtrl(machineModel, res, mode).search(shell);
})

router.get("/categories", async (req, res) => {
  let shell = await shellMaker(req.query);
  new genCtrl(categoryModel, res, mode).readItems(shell);
})

router.get("/classified", async (req, res) => {
  let shell = await shellMaker(req.query);
  shell.data.status = true
  new genCtrl(advertModel, res, mode).readItems(shell);
})

router.get("/subscription-packages", async (req, res) => {
  let shell = await shellMaker(req.query);
  shell.data.status = true;
  new genCtrl(subModel, res, mode).readItems(shell);
})

router.use("*", (req, res, next) => {
  let token = req.body.token || req.query.token;
  try {
    req.decoded = jwt.verify(token, process.env.USER_JWT_SECRET);
    next();
  } catch (error) {
    res.send({
      message: "Verification failed. Please login again.",
      code: 401,
      error: true
    });
  }
});

router.get("/profile", async (req, res) => {
  let shell = await shellMaker(req.query);
  shell.data = {
    _id: req.decoded.user_id
  }
  shell.fields = "-password -recovery_url";
  new genCtrl(ownerModel, res, mode).readSingleItem(shell);
})

router.put("/profile", async (req, res) => {
  let shell = await shellMaker(req.body);
  shell.data._id = req.decoded.user_id;
  delete shell.data.email
  delete shell.data.password
  new genCtrl(ownerModel, res, mode).updateItem(shell);
})

router.delete("/profile", async (req, res) => {
  let shell = await shellMaker(req.body);
  shell.data = {
    _id: req.decoded.user_id
  }
  new genCtrl(ownerModel, res, mode).deleteItem(shell);
})

// router.get("/owners", async (req, res) => {
//   let shell = await shellMaker(req.query);
//   new genCtrl(ownerModel, res, mode).readItems(shell);
// });

router.post("/machine", async (req, res) => {
  let shell = await shellMaker(req.body)
  shell.data.owner_id = req.decoded.user_id;
  new genCtrl(machineModel, res, mode).addItem(shell);
});

router.get("/own-machines", async (req, res) => {
  let shell = await shellMaker(req.query);
  shell.data.owner_id = req.decoded.user_id;
  new genCtrl(machineModel, res, mode).readItems(shell);
})

router.get("/count-machines", async (req, res) => {
  let shell = await shellMaker(req.query);
  new genCtrl(machineModel, res, mode).count({
    data: {
      owner_id: req.decoded.user_id
    }
  })
})

router.put("/machine", async (req, res) => {
  let shell = await shellMaker(req.body);
  shell.find.owner_id = req.query.user_id;
  new genCtrl(machineModel, res, mode).updateItem(shell);
});

router.delete("/machine", async (req, res) => {
  let shell = await shellMaker(req.body);
  new genCtrl(machineModel, res, mode).deleteItem(shell);
})

router.post("/initialize-payment", (req, res) => {
  new PaymentCtrl().initiatePayment(req, res);
})

router.get("/subscription", async (req, res) => {
  let shell = await shellMaker(req.query);
  new genCtrl(subModel, res, "development").readSingleItem(shell)
})

router.get("/check-subscription", async (req, res) => {
  // let shell = await shellMaker(req.query)
  // shell["data"]["_id"] = req.decoded.user_id;
  let shell = {
    data: {
      _id: req.decoded.user_id
    }
  }
  new subCtrl(res).checkSubscription(shell);
})

// confirm payment and then subscribe
router.post("/subscribe", async (req, res) => {
  let shell = await shellMaker(req.body)
  shell.data.user_id = req.decoded.user_id;
  new subCtrl(res).subscribe(shell);
})





module.exports = router;